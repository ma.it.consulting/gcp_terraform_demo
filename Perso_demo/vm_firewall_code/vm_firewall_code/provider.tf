terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.46.0"
    }
  }
}
provider "google" {
    project = "moonlit-triumph-370607"
    region = "europe-west2-a"
    credentials = "keys.json"
}