terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.52.0"
    }
  }
}
provider "google" {
    project = "directed-galaxy-370607"
    region  = "asia-east1"
    zone = "asia-east1-a"
   #credentials = "key.json"
}