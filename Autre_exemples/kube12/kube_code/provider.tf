terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.46.0"
    }
  }
}

provider "google" {
    project = "nifty-catfish-370607"
    region  = "us-central1"
    zone = "us-central1-a"
    credentials = "key.json"
}
