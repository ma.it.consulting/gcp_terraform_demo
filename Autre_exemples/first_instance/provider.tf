terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.85.0"
    }
  }
}

provider "google" {
    project = "moonlit-triumph-370607"
    region = "us-central1"
    zone = "us-central1-c"
    credentials = "keys.json"
}
